﻿using System;
using System.Collections.Generic;
using System.Text.RegularExpressions;
using com.chwar.xrui;
using com.chwar.xrui.UIElements;
using de.ugoe.cs.vivian.core;
using UnityEngine;
using UnityEngine.UIElements;

public class AppController : MonoBehaviour
{
    /// <summary>
    /// The main menu of the app
    /// </summary>
    public UIDocument menu;
    /// <summary>
    /// The UI of the app at runtime
    /// </summary>
    public UIDocument appUi;
    /// <summary>
    /// The return button that shows the main menu
    /// </summary>
    public UIDocument returnButton;
    /// <summary>
    /// Prefab to instantiate Vivian
    /// </summary>
    public GameObject vivianFrameworkPrefab;
    /// <summary>
    /// Object Positioner (for AR only)
    /// </summary>
    public ObjectPositioner objectPositioner;
    /// <summary>
    /// VP given to Vivian dynamically from the menu
    /// </summary>
    private VirtualPrototype _virtualPrototype;
    /// <summary>
    /// The return button of the main menu
    /// </summary>
    private Button _menuReturnBtn;

    /// <summary>
    /// List of prototypes to reference in the app
    /// </summary>
    public List<PrototypeUrls> prototypeUrls;
    
    [Serializable]
    public struct PrototypeUrls
    {
        public string bundleURL;
        public string prototypePrefabName;
    };
    
    public class PrototypeUrlsJsonWrapper
    {
        public List<PrototypeUrls> prototypes;

        public PrototypeUrlsJsonWrapper()
        {
            prototypes = new List<PrototypeUrls>();
        }
    }
    
    void Start()
    {
        // Deactivate OP to avoid raycasts
        if(objectPositioner != null)
            objectPositioner.enabled = false;
        
        _menuReturnBtn = menu.rootVisualElement.Q<Button>("Return");
        Button returnBtn = returnButton.rootVisualElement.Q<Button>("ReturnToMenu");
        returnBtn.clicked += () => ToggleMenu(true);
        
        DrawMenu();
    }

    /// <summary>
    /// Draws the buttons of the main menu.
    /// </summary>
    public void DrawMenu()
    {
        var xrui = menu.GetComponent<XRUIMenu>();
        xrui.RemoveAllElements();
        xrui.UpdateSubtext("");
        _menuReturnBtn.visible = false;

        menu.rootVisualElement.Q<ScrollView>("MainContainer").verticalPageSize = 0;

        // Create 2 buttons: to load prototypes locally or from a remote URL
        if (xrui.AddElement().ElementAt(0) is Button btn)
        {
            btn.text = "Load a local prototype";
            btn.clicked += ListLocalPrototypes;
        }
        if (xrui.AddElement().ElementAt(0) is Button btn2)
        {
            btn2.text = "Load a remote prototype";
            btn2.clicked += OpenURLModal;
        }    
    }

    /// <summary>
    /// Shows a modal to enter the URL of a remote server
    /// </summary>
    /// <exception cref="NotImplementedException"></exception>
    private void OpenURLModal()
    {
        XRUI.Instance.CreateModal("UrlModal", typeof(UrlModal));
        _menuReturnBtn.clicked -= OpenURLModal;
    }

    /// <summary>
    /// Shows all local prototypes
    /// </summary>
    private void ListLocalPrototypes()
    {
        var xrui = menu.GetComponent<XRUIMenu>();
        xrui.RemoveAllElements();
        xrui.UpdateSubtext("Local prototypes");
        _menuReturnBtn.visible = true;
        _menuReturnBtn.clicked += DrawMenu;
        
        // Create buttons from prototypes info
        foreach (PrototypeUrls pUrl in prototypeUrls)
        {
            if (xrui.AddElement().ElementAt(0) is Button btn)
            {
                btn.text = Regex.Replace(pUrl.bundleURL, "([a-z])_?([A-Z])", "$1 $2");
                btn.clicked += () => LoadPrototype(pUrl.bundleURL, pUrl.prototypePrefabName);
            }
        }
    }

    /// <summary>
    /// Shows all local prototypes
    /// </summary>
    public void ListRemotePrototypes(List<PrototypeUrls> urls)
    {
        var xrui = menu.GetComponent<XRUIMenu>();
        xrui.RemoveAllElements();
        xrui.UpdateSubtext("Remote prototypes");
        _menuReturnBtn.visible = true;
        _menuReturnBtn.clicked += OpenURLModal;
        
        // Create buttons from prototypes info
        foreach (PrototypeUrls pUrl in urls)
        {
            if (xrui.AddElement().ElementAt(0) is Button btn)
            {
                btn.text = Regex.Replace(pUrl.bundleURL.Split('/')[^1], "([a-z])_?([A-Z])", "$1 $2");
                btn.clicked += () => LoadPrototype(pUrl.bundleURL, pUrl.prototypePrefabName);
            }
        }
    }

    /// <summary>
    /// Shows/Hides the main menu.
    /// </summary>
    /// <param name="bToggle"></param>
    private void ToggleMenu(bool bToggle)
    {
        this.appUi.rootVisualElement.visible = !bToggle;
        this.menu.rootVisualElement.visible = bToggle;
        _menuReturnBtn.visible = bToggle;
        if (bToggle && objectPositioner != null)
            objectPositioner.enabled = false;
    }

    /// <summary>
    /// Loads a prototype from the menu.
    /// </summary>
    /// <param name="url">URL of the prototype.</param>
    /// <param name="prefabName">Name of the prefab to use.</param>
    private void LoadPrototype(string url, string prefabName)
    {
        // Hide menu
        ToggleMenu(false);

        if (_virtualPrototype != null && _virtualPrototype.BundleURL != url)
        {
            Destroy(_virtualPrototype.gameObject);
            InstantiateVirtualPrototype(url, prefabName);
        }
        else if(_virtualPrototype == null)
            InstantiateVirtualPrototype(url, prefabName);
    }

    /// <summary>
    /// Instantiates a prototype by giving an URL and a prefab to the Vivian instance.
    /// </summary>
    /// <param name="url">URL of the prototype.</param>
    /// <param name="prefabName">Name of the prefab to use.</param>
    private void InstantiateVirtualPrototype(string url, string prefabName)
    {
        _virtualPrototype = Instantiate(vivianFrameworkPrefab).GetComponent<VirtualPrototype>();
        _virtualPrototype.BundleURL = url;
        _virtualPrototype.PrototypePrefabName = prefabName;
        if (objectPositioner != null)
        {
            objectPositioner.enabled = true;
            objectPositioner.ResetVirtualPrototype(_virtualPrototype);
        }
    }
}