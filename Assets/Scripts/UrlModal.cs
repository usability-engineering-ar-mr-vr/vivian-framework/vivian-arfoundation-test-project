using System;
using System.Collections;
using System.Linq;
using com.chwar.xrui;
using com.chwar.xrui.UIElements;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UIElements;

public class UrlModal : MonoBehaviour
{
    private XRUIModal _xruiModal;
    private UIDocument _uiDocument;
    private TextField _urlTextField;
    private AppController _appController;

    void Start() {
        _xruiModal = GetComponent<XRUIModal>();
        _uiDocument = GetComponent<UIDocument>();
        _xruiModal.ModalTitle.text = "Load remote prototypes";
        _appController = FindObjectOfType<AppController>();
        StartPage();
    }

    void StartPage() {
        _xruiModal.UpdateModalFlow("UrlModal", "MainContainer", () =>
        {
            _urlTextField = _uiDocument.rootVisualElement.Q<TextField>("Url");
        });
        
        _xruiModal.SetCancelButtonAction(() =>
        {
            _appController.DrawMenu();
            Destroy(_xruiModal.gameObject);
        });
        
        _xruiModal.SetValidateButtonAction(ShowRemoteContent);
        _xruiModal.ValidateButton.text = "Next";
        _urlTextField.value = PlayerPrefs.GetString("url");
    }

    void ShowRemoteContent()
    {
        StartCoroutine(GetRequest(_urlTextField.value));
    }

    /// <summary>
    /// Tries to fetch an index.json file at the given URL, to later fetch prototypes
    /// </summary>
    /// <param name="uri"></param>
    /// <returns></returns>
    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri + "/index.json"))
        {
            // Request and wait for the desired page.
            yield return webRequest.SendWebRequest();

            string[] pages = uri.Split('/');
            int page = pages.Length - 1;

            switch (webRequest.result)
            {
                case UnityWebRequest.Result.ConnectionError:
                case UnityWebRequest.Result.DataProcessingError:
                case UnityWebRequest.Result.ProtocolError:
                    Debug.LogError(pages[page] + ": Error: " + webRequest.error);
                    XRUI.Instance.ShowAlert(XRUI.AlertType.Danger,"Error", webRequest.error);
                    _xruiModal.SetFieldError(_urlTextField);
                    break;
                case UnityWebRequest.Result.Success:
                    // Save URL in player prefs for later reuse
                    PlayerPrefs.SetString("url", uri);
                    PlayerPrefs.Save();
                    
                    Debug.Log(pages[page] + ":\nReceived: " + webRequest.downloadHandler.text);
                    var json = JsonUtility.FromJson<AppController.PrototypeUrlsJsonWrapper>(webRequest.downloadHandler.text);
                    var json2 = new AppController.PrototypeUrlsJsonWrapper();
                    json.prototypes.ForEach(x =>
                    {
                        var p = new AppController.PrototypeUrls
                        {
                            bundleURL = uri + "/" + x.bundleURL,
                            prototypePrefabName = x.prototypePrefabName
                        };
                        json2.prototypes.Add(p);
                    });
                    _appController.ListRemotePrototypes(json2.prototypes);
                    Destroy(_xruiModal.gameObject);
                    break;
            }
        }
    }
}